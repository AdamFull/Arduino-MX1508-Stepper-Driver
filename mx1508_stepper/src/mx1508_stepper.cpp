//Make by AdamFull
//Repository: https://github.com/AdamFull/Arduino-MX1508-Stepper-Driver

#include "Arduino.h"
#include "mx1508_stepper.h"

mx1508::mx1508(int step_limit, int in1, int in2, int in3, int in4)
{
	step_number = 0;    // which step the motor is on
	last_step_time = 0; // time stamp in us of the last step taken
	max_steps = step_limit; // total number of steps for this motor

	input1 = in1;
	input2 = in2;
	input3 = in3;
	input4 = in4;

	useBreakPoint = false;

	pinMode(in1, OUTPUT);
	pinMode(in2, OUTPUT);
	pinMode(in3, OUTPUT);
	pinMode(in4, OUTPUT);
}

mx1508::mx1508(int step_limit, int in1, int in2, int in3, int in4, int break_point)
{
	step_number = 0;    // which step the motor is on
	last_step_time = 0; // time stamp in us of the last step taken
	max_steps = step_limit; // total number of steps for this motor

	input1 = in1;
	input2 = in2;
	input3 = in3;
	input4 = in4;
	l_br_pnt = break_point;

	useBreakPoint = true;

	pinMode(in1, OUTPUT);
	pinMode(in2, OUTPUT);
	pinMode(in3, OUTPUT);
	pinMode(in4, OUTPUT);
	pinMode(break_point, INPUT);
}

mx1508::mx1508(int step_limit, int in1, int in2, int in3, int in4, int l_break_point, int r_break_point)
{
	step_number = 0;    // which step the motor is on
	last_step_time = 0; // time stamp in us of the last step taken
	max_steps = step_limit; // total number of steps for this motor

	input1 = in1;
	input2 = in2;
	input3 = in3;
	input4 = in4;
	l_br_pnt = l_break_point;
	r_br_pnt = r_break_point;

	useBreakPoint = true;

	pinMode(in1, OUTPUT);
	pinMode(in2, OUTPUT);
	pinMode(in3, OUTPUT);
	pinMode(in4, OUTPUT);
	pinMode(l_break_point, INPUT);
	pinMode(r_break_point, INPUT);
}

void mx1508::setSpeed(long speed)
{
	current_speed = 60L * 1000L * 1000L / max_steps / speed;
}

void mx1508::step(int st, bool reverse)
{
	unsigned steps_left = st;
	while (steps_left > 0)
	{
		unsigned long timeNow = micros();
		if (timeNow - last_step_time >= current_speed)
		{
			last_step_time = timeNow;

			if(useBreakPoint)
				if (getLBreakPoint())
				{
					current_step = 0;
					step_number = 0;
					break;
				}
				else if (getRBreakPoint())
				{
					current_step = max_steps;
					step_number = max_steps;
					break;
				}

			if (reverse)
			{
				step_number++;
				current_step++;
				if (step_number == max_steps)
					step_number = 0;
			}
			else
			{
				if (step_number == 0)
					step_number = max_steps;
				step_number--;
				current_step--;
			}

			steps_left--;
			makeStep(step_number % 4);
		}
	}
}

void mx1508::makeStep(int step_part)
{
	switch (step_part)
	{
	case 0:
		digitalWrite(input1, LOW);
		digitalWrite(input2, LOW);
		digitalWrite(input3, HIGH);
		digitalWrite(input4, LOW);
		break;
	case 1:
		digitalWrite(input1, HIGH);
		digitalWrite(input2, LOW);
		digitalWrite(input3, LOW);
		digitalWrite(input4, LOW);
		break;
	case 2:
		digitalWrite(input1, LOW);
		digitalWrite(input2, LOW);
		digitalWrite(input3, LOW);
		digitalWrite(input4, HIGH);
		break;
	case 3:
		digitalWrite(input1, LOW);
		digitalWrite(input2, HIGH);
		digitalWrite(input3, LOW);
		digitalWrite(input4, LOW);
		break;
	}
}