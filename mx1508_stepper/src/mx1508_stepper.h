#ifndef mx1508_stepper_h
#define mx1508_stepper_h

class mx1508 {
public:
	// constructors:
	mx1508(int step_limit, int in1, int in2, int in3, int in4);
	mx1508(int step_limit, int in1, int in2, int in3, int in4, int break_point);
	mx1508(int step_limit, int in1, int in2, int in3, int in4, int l_break_point, int r_break_point);

	// speed setter method:
	void setSpeed(long whatSpeed);
	unsigned long getSpeed() { return current_speed; }

	int getCurrentStep() { return current_step; }

	bool getLBreakPoint() { return digitalRead(l_br_pnt); }
	bool getRBreakPoint() { return digitalRead(r_br_pnt); }

	// mover method:
	void step(int st, bool reverse);

private:
	void makeStep(int step_part);

	unsigned long current_speed;	// delay between steps, in ms, based on speed
	int max_steps;      // total number of steps this motor can take
	int step_number;          // which step the motor is on
	int current_step;			//Counted steps at the moment
	bool useBreakPoint;

	// motor pin, and trailer numbers:
	int	input1;
	int input2;
	int input3;
	int input4;
	int l_br_pnt;
	int r_br_pnt;

	unsigned long last_step_time; // time stamp in us of when the last step was taken
};

#endif