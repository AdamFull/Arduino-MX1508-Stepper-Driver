#include "mx1508_stepper.h"

#define STEP_LIMIT 256
#define L_BREAK_POINT_PIN 6
#define R_BREAK_POINT_PIN 7

mx1508 stepper(STEP_LIMIT, 2, 3, 4, 5, L_BREAK_POINT_PIN, R_BREAK_POINT_PIN);

void setup() 
{
  stepper.setSpeed(70);
  Serial.begin(9600);
}

void loop() 
{
  stepper.step(300, true);
  delay(1000);
  stepper.step(300, false);
  delay(1000);
  stepper.getLBreakPoint();
  stepper.getRBreakPoint();
  
}
