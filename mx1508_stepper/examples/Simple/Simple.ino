#include "mx1508_stepper.h"

#define STEP_LIMIT 256

mx1508 stepper(STEP_LIMIT, 2, 3, 4, 5);

void setup() 
{
  stepper.setSpeed(70);
  Serial.begin(9600);
}

void loop() 
{
  stepper.step(STEP_LIMIT, true);
  delay(1000);
  stepper.step(STEP_LIMIT, false);
  delay(1000);
  Serial.print("Current step: ");
  Serial.print(stepper.getCurrentStep());
  Serial.println("");
}