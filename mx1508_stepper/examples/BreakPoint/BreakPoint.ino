#include "mx1508_stepper.h"

#define STEP_LIMIT 256
#define BREAK_POINT_PIN 6

mx1508 stepper(STEP_LIMIT, 2, 3, 4, 5, BREAK_POINT_PIN);

void setup() 
{
  stepper.setSpeed(70);
  Serial.begin(9600);
}

void loop() 
{
  stepper.step(STEP_LIMIT, true);
  delay(1000);
  stepper.step(STEP_LIMIT, false);
  delay(1000);
  Serial.print("Is break point pressed: ");
  Serial.print(stepper.getLBreakPoint());
  Serial.println("");
  
}