# Arduino-MX1508-Stepper-Driver

## How to use?

1) include header file
```C++
#include "mx1508_stepper.h"
```
2) Set the number of steps per turn
```C++
#define STEP_LIMIT 256
```
3) Create a class object with driver pins
```C++
mx1508 stepper(STEP_LIMIT, 2, 3, 4, 5);
```
4) You can configure stepper speed
```C++
  stepper.setSpeed(70);
```
5) And in loop
```C++
stepper.step(STEP_LIMIT, true);
delay(1000);
stepper.step(STEP_LIMIT, false);
delay(1000);
```
